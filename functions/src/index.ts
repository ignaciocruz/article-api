import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const app = express();
const main = express();

main.use('/api/v1', app);
main.use(bodyParser.json());

export const webApi = functions.https.onRequest(main);

app.get('/warmup', (request, response) => {

  response.send('Warming up friend.');

})

app.post('/articles', async (request, response) => {
  try {
    const { name, quantity, description, price } = request.body;
    const data = {
      name,
      quantity,
      description,
      price
    }
    const articleRef = await db.collection('articles').add(data);
    const article = await articleRef.get();

    response.json({
      id: articleRef.id,
      data: article.data()
    });

  } catch (error) {

    response.status(500).send(error);

  }
});


app.get('/articles', async (request, response) => {
  try {

    const articleQuerySnapshot = await db.collection('articles').get();
    const articles = [];
    articleQuerySnapshot.forEach(
      (doc) => {
        articles.push({
          id: doc.id,
          data: doc.data()
        });
      }
    );

    response.json(articles);

  } catch (error) {

    response.status(500).send(error);

  }

});
